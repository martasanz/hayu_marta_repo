const path = require("path");
const webpack = require("webpack");

// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const CSSModuleLoader = {
//   loader: "css-loader",
//   options: {
//     modules: true,
//     sourceMap: true,
//     localIdentName: "[local]__[hash:base64:5]",
//     minimize: true,
//   },
// };

// const CSSLoader = {
//   loader: "css-loader",
//   options: {
//     modules: false,
//     sourceMap: true,
//     minimize: true,
//   },
// };

// const postCSSLoader = {
//   loader: "postcss-loader",
//   options: {
//     ident: "postcss",
//     sourceMap: true,
//     plugins: () => [
//       autoprefixer({
//         browsers: [">1%", "last 4 versions", "Firefox ESR", "not ie < 9"],
//       }),
//     ],
//   },
// };

module.exports = {
  entry: "./src/index.js",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] },
      },
      {
        test: /\.(scss|css)$/,
        exclude: /node_modules/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      // {
      //   test: /\.(scss|css)$/,
      //   exclude: /node_modules/,
      //   use: [
      //     "style-loader",
      //     "sass-loader",
      //     {
      //       loader: "css-loader",
      //       options: {
      //         modules: true,
      //       },
      //     },
      //   ],
      // },
      // {
      //   test: /\.scss$/,
      //   use: [
      //     "css-loader", // translates CSS into CommonJS
      //     "style-loader", // creates style nodes from JS strings
      //     "sass-loader", // compiles Sass to CSS, using Node Sass by default
      //   ],
      // },
      // {
      //   test: /\.css$/i,
      //   exclude: /node_modules/,
      //   use: [
      //     "style-loader",
      //     {
      //       loader: "css-loader",
      //       options: {
      //         modules: true,
      //       },
      //     },
      //   ],
      // },

      // {
      //   test: /\.scss$/,
      //   exclude: /\.module\.scss$/,
      //   use: ["style-loader", CSSLoader, postCSSLoader, "sass-loader"],
      // },
      // {
      //   test: /\.module\.scss$/,
      //   use: ["style-loader", CSSModuleLoader, postCSSLoader, "sass-loader"],
      // },
    ],
  },

  resolve: { extensions: ["*", ".js", ".jsx", ".scss"] },
  output: {
    path: path.resolve(__dirname, "dist/"),
    publicPath: "/dist/",
    filename: "bundle.js",
  },
  devServer: {
    contentBase: path.join(__dirname, "public/"),
    port: 3000,
    publicPath: "http://localhost:3000/dist/",
    historyApiFallback: true,
    hotOnly: true,
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
};
