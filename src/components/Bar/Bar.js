import styles from "./Bar.scss";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";

function Bar(props) {
  console.log(props);
  console.log("classes", styles);
  // const itemsMenu = props.menu.items[(1, "text")];
  console.log("error items", props.menu);
  const itemsArray = props.menu.items.map((item) => {
    return item.text;
  });
  const [firstItem, secondItem, thirdItem] = itemsArray; //destructuring into each elemnt of the array
  // console.log(`ItemsArray${itemsArray}`);

  return (
    <header className={styles.topBarContainer}>
      <div className={styles.hayuLogoBar}>
        <NavLink to="/#">
          <img src={props.menu.logo} alt="Logo img" />
        </NavLink>
      </div>
      <div className={styles.menuBar}>
        <NavLink to="/about-dogs" className={styles.menuBarBox}>
          {firstItem}
        </NavLink>
        <NavLink to="/search-breed" className={styles.menuBarBox}>
          {secondItem}
        </NavLink>
        <NavLink to="/more-dogs" className={styles.menuBarBox}>
          {thirdItem}
        </NavLink>
      </div>
      {/* <nav>
        <ul className={classes.menuBar}>
          <li className={classes.menuBarBox}>'Hi there'</li>
          <li className={classes.menuBarBox}>'Hi the'</li>
          <li className={classes.menuBarBox}>'Hi t'</li>
        </ul>
      </nav> */}
    </header>
  );
}

export default Bar;
