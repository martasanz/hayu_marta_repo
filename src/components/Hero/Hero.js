import classes from "./Hero.scss";
import React from "react";

const Hero = (props) => {
  console.log("props Her", props);
  const title = props.hero.title;
  const heroImg = props.hero.heroImg; //from Home . myHayu.json
  // const heroImg = props.dogs; //from Home Dogs API
  // console.log("This ", heroImg);
  // const image = heroImg.map((item) => {
  //   return item.image.url;
  // });
  // const [images2] = image;
  // console.log("data in hero map", images2);

  const heroImgStyled = {
    backgroundImage: `url(${heroImg})`,
    // backgroundImage: `url(${heroImg})`,

    top: "100px",
    width: "100%",
    height: "600px",
    backgroundRepeat: "no-repeat",
    // overflow: scroll,
  };
  return (
    <div className={classes.imgBox}>
      <div className={classes.imgBox} style={heroImgStyled} />
      <h3>{title}</h3>
    </div>
  );
  //   <div className={classes["img-box"]}>{title}</div>;)
};

export default Hero;
