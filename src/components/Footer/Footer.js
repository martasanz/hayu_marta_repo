import myHayu from "../myHayu.json";
import React, { Fragment } from "react";
import style from "./Footer.scss";
import FooterBackground from "./FooterBackground";
import FooterItems from "./FooterItems";

function Footer() {
  const footerMain = myHayu.footer;
  const footerItems = footerMain.items;
  return (
    <Fragment>
      <FooterBackground footerMain={footerMain}></FooterBackground>
      {/* <FooterItems footerItems={footerItems} /> */}
      {/* <div className={style.mainFooterLogo}>{footerLogo}</div> */}
    </Fragment>
  );
}

export default Footer;
