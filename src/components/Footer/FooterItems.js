import React from "react";
import style from "./Footer.scss";

// function FooterItems(props) {
//   console.log(`props HEY`, props);

//   const itemsFooter = props.footerItems;
//   console.log("heyT", itemsFooter);
//   return (
//     <div className={style.mainFooter}>
//       {itemsFooter.map((eachFooterItem) => (
//         <ul className={style.mainFooterUl} key={eachFooterItem.text}>
//           <li>
//             <p className={style.mainFooterP} url={eachFooterItem.url} />
//           </li>
//         </ul>
//       ))}
//     </div>
//   );
// }

function FooterItems(props) {
  console.log(`props HEY`, props);

  const itemsFooter = props.itemsFooter.items;
  console.log("heyT", itemsFooter);
  return (
    <div className={style.mainFooter}>
      <ul className={style.mainFooterUl}>
        {itemsFooter.map((eachFooterItem) => (
          <li key={eachFooterItem.url}>{eachFooterItem.text}</li>
        ))}
      </ul>
      <p>Copyright &copy; 2021 Hayu's Puppies</p>
    </div>
  );
}

export default FooterItems;
