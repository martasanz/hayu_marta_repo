import style from "./Footer.scss";
import React from "react";
import FooterItems from "./FooterItems";

const FooterBackground = (props) => {
  const logo = props.footerMain.logo;
  console.log("hereeeeee logo", logo);
  const footerBackgroundImg = props.footerMain.footerBackgImg;
  console.log(`This ${footerBackgroundImg}`);

  const FooterBackgroundImgStyled = {
    backgroundImage: `url(${footerBackgroundImg})`,
    // z-index: 1,
    // background-repeat: repeat,
    // background-position: center,
    width: "100%",
    height: "300px",
    // border-radius: 10px,
  };
  return (
    <div>
      <div className={style.mainFooter} style={FooterBackgroundImgStyled}>
        <div className={style.mainFooterLogo}>
          <img src={logo.url} alt="Logo img" />
        </div>
        <FooterItems itemsFooter={props.footerMain}></FooterItems>
      </div>

      {/* <h3>{title}</h3> */}
    </div>
  );
  //   <div className={classes["img-box"]}>{title}</div>;)
};

export default FooterBackground;
