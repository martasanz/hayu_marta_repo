import classes from "./Carousel.scss";
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

//TODO: the link of each carousel box is here
function Carousel(props) {
  //   console.log(props);
  const title = props.carousel.title;
  //   console.log(title);
  const itemsScrollBox = props.carousel.items;

  const propsDogs = props.dogs; //from Home Dogs API
  console.log("props dogs carousel", propsDogs); //its good

  const imagex = propsDogs.map((item) => {
    return item.image;
  });
  console.log("IMAGE carousel", imagex);

  //Extract the Object from the array modules
  const itemsArray = props.carousel.map((item) => {
    return item;
  });
  const [carouselInfo] = itemsArray; //destructuring into the object of the array

  return (
    <div className={classes.wholeScrollBox}>
      <div className={classes.displayTitleBox}>{carouselInfo.title}</div>
      <div className={classes.scrollBox}>
        {/* //extracts from the array items each Sample picture */}
        {imagex.map((eachImgScrollBox) => (
          <ul key={eachImgScrollBox.id} className={classes.scrollBoxItem}>
            <li>
              <Link to="/search-breed/breedId">
                <img
                  className={classes.scrollBoxItemImag}
                  src={eachImgScrollBox.url}
                  alt={"Random Dog Pic"}
                />
              </Link>
            </li>
          </ul>
        ))}
      </div>
    </div>
  );
}

export default Carousel;
