import { Route, Switch, useParams, useRouteMatch } from "react-router";
import React, { useState, useEffect } from "react";
import styles from "./DogInfo.scss";
import axios from "axios";
import { Link } from "react-router-dom";
import Breed from "./Breed";

//TODO: I have mixed Breed and BreedInfo, to try routing to one or other
function BreedInfo(props) {
  const { breedId } = useParams();

  const { url } = useRouteMatch();

  console.log("params", breedId);
  const propsBreed = props.dogs; //from App.js Dogs API
  console.log("props BreedInfo", propsBreed);

  const breedList = propsBreed.map((eachDogBreed) => {
    return (
      <li key={eachDogBreed.id}>
        {/* ${url}/ */}
        <Link to={`${eachDogBreed.id}`}> {eachDogBreed.name}</Link>
      </li>
    );
  });
  console.log("ID BreedInfo", breedList);

  return (
    <div>
      {/* <li>{dogs}</li> */}
      <div className={styles.boxTitle}>{breedId}</div>
      <ul>{breedList}</ul>
      {/* {propsBreed.map((eachDogBreed) => (
        <ul key={eachDogBreed.id}>
          <li>
          <h1>{eachDogBreed.name}</h1>
          </li>
        </ul>
      ))} */}
      <Route path={`${url}/:breedId`}>
        <Breed propsBreed={propsBreed} />
      </Route>
    </div>
  );
}

export default BreedInfo;
