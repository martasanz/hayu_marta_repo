import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styles from "./StylePages.scss";
import ReactPlayer from "react-player";
import {
  Link,
  Route,
  Switch,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import BreedInfo from "./BreedInfo";
import Breed from "./Breed";

//TODO: It redirects in the path right but it doesnt show any info
function SearchBreed(props) {
  // SearchBreed.propTypes = { searchBreedId: PropTypes.string.isRequired };
  // const { path } = useRouteMatch();
  const { breedId } = useParams();

  // //SEARCH FORM
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const inputHandler = (event) => {
    setSearchTerm(event.target.value);
  };

  console.log("params", breedId);
  const propsBreed = props.dogs; //from App.js Dogs API
  console.log("props BreedInfo", propsBreed);

  const breedList = propsBreed.map((eachDogBreed) => {
    return (
      <li key={eachDogBreed.id}>
        {/* ${path}/ */}
        <Link to={`breed/${eachDogBreed.id}`}> {eachDogBreed.name}</Link>
      </li>
    );
  });
  console.log("ID BreedInfo", breedList);

  const breedName = propsBreed.map((eachDogBreed) => {
    return eachDogBreed.name;
  });
  console.log(" BreedName", breedName);

  useEffect(() => {
    const results = breedName.filter((breed) =>
      breed.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSearchResults(results);
  }, [searchTerm]);

  return (
    <div style={{ position: "absolute", margin: "20px" }}>
      <h2 style={{ marginTop: "120px", marginLeft: "20px" }}>
        Search Breed page
      </h2>
      <input
        type="text"
        placeholder="Search breed"
        value={searchTerm}
        onChange={inputHandler}
        // style={{ marginTop: "100px" }}
      />
      <div>
        <div className={styles.boxTitle}>{breedId}</div>
        <ul style={{ listStyle: "none", marginLeft: "20px" }}>
          {searchResults.map((breed) => (
            // eslint-disable-next-line react/jsx-key
            <li>{breed}</li>
          ))}
          {/* {searchResults.map((breed) => {
            return breed;
          })} */}
        </ul>
        <ul style={{ listStyle: "none", marginLeft: "20px" }}>{breedList}</ul>
      </div>

      {/* <li key={breed.id}>{breed}</li> */}
      {/* <Route path={`${path}/:breedId`}>
        <Breed propsBreed={propsBreed} />
      </Route> */}
    </div>
  );
}

export default SearchBreed;
