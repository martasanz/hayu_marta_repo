import myHayu from "../../components/myHayu.json";
import React, { Fragment, useEffect, useState } from "react";
import Bar from "../../components/Bar/Bar";
import Hero from "../../components/Hero/Hero";
import Carousel from "../../components/Carousel/Carousel";
import axios from "axios";
import BreedInfo from "../BreedInfo";

function Home() {
  const [hero, carousel] = [myHayu.hero, myHayu.modules];
  // const menu = myHayu.header;
  // const hero = myHayu.hero;
  // const carousel = myHayu.modules;

  const [dogs, setDogs] = useState([]);

  useEffect(() => {
    axios
      .get("https://api.thedogapi.com/v1/breeds?limit=10&page=0", {
        headers: { api_key: "3a73531c-d934-42a2-abf0-4e08c892cd15" },
      })
      .then((response) => {
        console.log("then response", response.data);
        const responseData = response.data;
        const image = response.data.map((item) => {
          return item.image;
        });
        const [images2] = image;
        console.log("new data", images2);
        // setDogs(images2);
        const checkSet = setDogs(responseData);
        console.log("setDogs", checkSet);
      })
      .catch((error) => {
        console.log("error", error);
      });
  }, []);

  return (
    <Fragment>
      {/* <Hero hero={hero} /> */}
      <Hero hero={hero} dogs={dogs} />
      <Carousel carousel={carousel} dogs={dogs} />
      {/* <BreedInfo dogs={dogs} /> ??? */}
      {/* <main className={classes.home}>{props.children}</main> */}
      {/* <main className="">{props.children}</main> */}
    </Fragment>
  );
}
export default Home;
