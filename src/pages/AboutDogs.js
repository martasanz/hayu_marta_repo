import React from "react";
import PropTypes from "prop-types";
import styles from "./StylePages.scss";
import ReactPlayer from "react-player";

function AboutDogs(aboutDogsId) {
  AboutDogs.propTypes = { aboutDogsId: PropTypes.string.isRequired };
  return (
    <div>
      <h2>About Dogs page</h2>
      <ReactPlayer
        url="https://www.youtube.com/watch?v=ltg2J4geVbQ"
        className={styles.videoResponsive}
        width="853"
        height="480"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        title="About Dogs youtube"
      />
      {/* <iframe
        className={styles.videoResponsive}
        width="853"
        height="480"
        src={`https://www.youtube.com/watch?v=ltg2J4geVbQ ${aboutDogsId}`}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        title="About Dogs youtube"
      /> */}
    </div>
  );
}

export default AboutDogs;
