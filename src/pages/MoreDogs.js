import React from "react";
import { Link } from "react-router-dom";

import PropTypes from "prop-types";
// import styles from "./StylePages.css";
import ReactPlayer from "react-player";

function MoreDogs(moreDogsId) {
  //   MoreDogs.propTypes = { moreDogsId: PropTypes.string.isRequired };
  return (
    <div>
      {/* <ReactPlayer
        url="https://www.youtube.com/watch?v=D4_1taEjCms"
        className={styles.videoResponsive}
        width="853"
        height="480"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        title="About Dogs youtube"
      /> */}
      <section>
        <h2>More Dogs page</h2>
        <br />
        <br />
        <br />
        <ul>
          <li>
            <Link to="more-dogs/golden">Golden</Link>
          </li>
          <li>
            <Link to="more-dogs/corgi">Corgi</Link>
          </li>
          <li>
            <Link to="more-dogs/pinscher">Pinscher</Link>
          </li>
        </ul>
      </section>
    </div>
  );
}

export default MoreDogs;
