import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

const Breed = () => {
  //   const breed = data.find((p) => p.id === Number(breedId));
  const { breedId } = useParams();
  const [breed, setBreed] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(`https://api.thedogapi.com/v1/breeds/${breedId}`, {
        headers: { api_key: "3a73531c-d934-42a2-abf0-4e08c892cd15" },
      })
      .then((response) => {
        const responseData = response.data;
        console.log("response", response);

        // const transformedData = responseData.map((breedData) => {
        //   return {
        //     responseData: breedData,
        //     breedFor: breedData.breed_for,
        //   };
        // });

        setBreed(responseData);
        setLoading(false);
      })
      .catch((error) => {
        console.log("error", error);
      });
  }, []);

  if (loading) {
    return "...Loading";
  }

  console.log("breed from Breed", breed);
  console.log("breedName from Breed", breed.name);
  console.log("breedId from Breed", breedId);
  return (
    <div>
      <div style={{ position: "absolute", margin: "20px" }}>
        <h1 style={{ marginTop: "120px", marginLeft: "20px" }}>BRED PAGE</h1>
        <h3>{breed.name}</h3>
        <h3>{breed.temperament}</h3>
        <h3>{breed.breedFor}</h3>

        <img src={breed.reference_image_id} alt=""></img>
      </div>

      {/* <p>{breed.weight}</p>
      <p>{breed.origin}</p> */}
    </div>
  );
};

export default Breed;
