import { useParams } from "react-router";
import React, { useState } from "react";
import styles from "./DogInfo.scss";

function DogInfo(props) {
  const params = useParams();
  console.log("dooooo", props.dogDetail);
  const dogDetailIdArray = props.dogDetail.map((dog) => {
    return dog.text;
  });
  const [golden, corgi, pinscher] = dogDetailIdArray;

  console.log(params === "corgi");
  console.log(props.button.clicked);
  console.log(props.button.notClicked);

  const [isLiked, setIsLiked] = useState(false);
  const likedFunct = () => {
    setIsLiked(true);
    console.log(isLiked);
  };
  let textDetailDog = golden;
  let imgDogDetail =
    " https://www.zooplus.es/magazine/wp-content/uploads/2017/03/gl%C3%BCcklich-golden-retriever-welpe-1024x682.jpg";
  if (params.dogId === "golden") {
    textDetailDog = golden;
    imgDogDetail =
      "https://www.zooplus.es/magazine/wp-content/uploads/2017/03/gl%C3%BCcklich-golden-retriever-welpe-1024x682.jpg";
  } else if (params.dogId === "corgi") {
    textDetailDog = corgi;
    imgDogDetail =
      "http://thepetworldapp.com/wp-content/uploads/2020/03/Imagen_3_corgi.jpg";
  } else if (params.dogId === "pinscher") {
    textDetailDog = pinscher;
    imgDogDetail =
      "https://petyzoo.com/wp-content/uploads/2017/09/pinscher-perro-raza.jpg";
  }

  return (
    <section className={styles.container}>
      <h1>Dog Details</h1>
      {/* <p>{params.dogId}</p> */}
      <div className={styles.box}>
        <div className={styles.boxTitle}> {params.dogId} </div>
        <br />
        <p className={styles.boxText}>{textDetailDog}</p>
      </div>

      <div>
        <img className={styles.imgBox} src={imgDogDetail} alt=""></img>
      </div>

      <div className={styles.buttonBox}>
        {isLiked && (
          <img className={styles.buttonBoxImg} src={props.button.clicked} />
        )}
        {!isLiked && (
          <img
            className={styles.buttonBoxImg}
            src={props.button.notClicked}
            onClick={likedFunct}
          />
        )}
        <div className={styles.buttonBoxText}>
          Click me if you enjoyed the post!
        </div>
      </div>

      {/* CLICKING on an image and moving to the next */}
      {/* <div>
        <img
          alt=""
          src="http://clipart-library.com/clipart/rcLoj94bi.htm"
          // style="height:20px; width:20px;"
          id="imgLike"
          onClick={likedFunct}
        />
      </div> */}

      {/* <div>
        <img src={"../../../images/IMG_1457.png"}></img>
      </div> */}
    </section>
  );
}

export default DogInfo;
