import { Route, Router, Link, Switch } from "react-router-dom";
import React, { Component, Fragment, useEffect, useState } from "react";
import axios from "axios";

import myHayu from "./components/myHayu.json";
import myDogInfo from "./pages/myDogInfo.json";
import Footer from "./components/Footer/Footer";
import Home from "./pages/Home/Home";
import Bar from "./components/Bar/Bar";
import AboutDogs from "./pages/AboutDogs";
import SearchBreed from "./pages/SearchBreed";
import MoreDogs from "./pages/MoreDogs";
import DogInfo from "./pages/DogInfo";
import BreedInfo from "./pages/BreedInfo";
import Breed from "./pages/Breed";

//TODO: Can you pass it the elements without having to do the useEffect here and everything
function App() {
  // class App extends Component {
  const [menu] = [myHayu.header];
  const [button, dogDetail] = [myDogInfo.button, myDogInfo.dogDetail];
  console.log("app dd", dogDetail);

  const [dogs, setDogs] = useState([]);

  useEffect(() => {
    axios
      .get("https://api.thedogapi.com/v1/breeds      ", {
        headers: { api_key: "3a73531c-d934-42a2-abf0-4e08c892cd15" },
      })
      .then((response) => {
        const responseData = response.data;
        setDogs(responseData);
      })
      .catch((error) => {
        console.log("error", error);
      });
  }, []);

  // render() {
  return (
    <div>
      <Bar menu={menu} />
      <Switch>
        <Route exact path="/">
          <Home />
          <Footer />
        </Route>
        <Route path="/about-dogs">
          <AboutDogs />
        </Route>
        <Route exact path="/search-breed">
          <SearchBreed dogs={dogs} />
        </Route>
        <Route path="/breed/:breedId">
          <Breed />
        </Route>
        <Route path="/more-dogs" exact>
          <MoreDogs />
        </Route>
        <Route path="/more-dogs/:dogId">
          <DogInfo button={button} dogDetail={dogDetail} />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
